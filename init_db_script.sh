#!/bin/bash
echo "Setting up MariaDB"
/usr/bin/mysqld_safe & sleep 10
mysql -e "CREATE DATABASE web;"
mysql -e "CREATE USER 'web'@'localhost' IDENTIFIED BY 'password';"
mysql -e "GRANT ALL ON web.* TO 'web'@'localhost';"
killall mysqld
sleep 10